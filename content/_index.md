## Front Page Content

## Functionally Confused
I was doing this problem https://hyperskill.org/learn/step/9577 over at jetbrains academy. I initially tried to do this:
https://gitlab.com/codetastic/learning-python/-/blob/master/temp_conversion1.py
but that resulted in this error:
https://gitlab.com/codetastic/learning-python/-/blob/master/temp_conversion_error.txt
So I was stumped for a long time on why it was happening. Then on a whim I decided to just remove the parameters and suddenly it worked in this final form: https://gitlab.com/codetastic/learning-python/-/blob/master/temp_conversion2.py
Confusing as heck but glad I am past it now.

## Learning and practicing 
I've been using hyperskill.org to learn and practice more so than any other source now. I like the way it is presented and I feel it is challenging
but not overwhelming. Still not sure what I'm blogging about as I've never done a blog before but I'll try to keep track and update this thing 
once in awhile. My only major hurdle so far was getting stumped on the multiple choice excercise involving XOR at https://hyperskill.org/learn/step/6029. I couldn't wrap my head around it after trying for awhile so I just clicked the postpone button and I'll try getting back to it later.

## Busy But Still At It
Haven't been able to catch the Beginbot stream the past week but I am still plugging away at learning. I'm starting to feel like I understand
a little tiny bit but I still feel like I am groping in the dark when I try to write something from scratch without guidance.

## Learning Python Update
Ok seems like I figured out my while loop problem. I couldn't seem to figure out how to make it accept N or n to stop the loop.
At first I was trying while end_loop != "N" or "n":
Well it just wouldn't work. I guess that isn't an appropriate way to use "or". So I thought about it for a bit and decided why can't I just force it to be a capital N?
Because I had it working when just asking for a "N" to end the loop. Adding the or "n" is what caused the problem. So I dug a little bit and came across upper(). With the 
addition of upper() I can now make the user input for end_loop always be a capital and sucessfully end the loop.

[scorekeepercsvlib.py](https://gitlab.com/codetastic/learning-python/-/blob/master/scorekeepercsvlib.py)

## Learning Python
This is what I've been up to so far:
Reading a few different books including Python 3 The Hard Way, Head First Learn to Code, and Automate the Boring Stuff.
Doing a non book related project given to me by another twitch user (danielfeldroy) with a base script (a little script to keep a record of scores in games) and a
set of TODO to practice with. Currently stumped on how to get it to work properly with a while loop.

[scorekeeper.py](https://gitlab.com/codetastic/learning-python/-/blob/master/scorekeeper.py) is the original non looping version that I added the functionality to include the name of the game

[scorekeeperloop.py](https://gitlab.com/codetastic/learning-python/-/blob/master/scorekeeperloop.py) is the script I am currently working on

[score.csv](https://gitlab.com/codetastic/learning-python/-/blob/master/score.csv) is sample output



# Ignore stuff below this
This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
